<?php 

	class Leave_Model extends CI_Model{

		public function get_data()
		{
			$this->db->select('tbl_leave.*,tbl_staff.staff_name');
			$this->db->from('tbl_staff');
			$this->db->join('tbl_leave','tbl_staff.staff_id = tbl_leave.leave_staff_id ');
			$query = $this->db->get();

			return $query->result();
		}

		// this function is used to store leave

		public function store_leave()
		{
			$data = array(
				'leave_staff_id' =>$this->input->post('staff'),
				'leave_date' =>$this->input->post('date_time')
				 );
			$this->db->insert('tbl_leave',$data);
			
		}
	}


 ?>