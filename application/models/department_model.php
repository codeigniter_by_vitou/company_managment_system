<?php 

	class department_model extends CI_Model{

		public function get_data()
		{
			$this->db->select('*');
			$this->db->from('tbl_department');
			$query = $this->db->get();

			return $query->result();
		}

		// this function is used to store department

		public function store_department()
		{
			$data = array(
				'dep_title' =>$this->input->post('department_title') ,
				'dep_desc' =>$this->input->post('department_desc'),
				'dep_create_at' => date('Y-m-d H:i:s')
				 );
			$this->db->insert('tbl_department',$data);
			
		}
	}


 ?>