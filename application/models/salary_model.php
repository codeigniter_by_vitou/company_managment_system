<?php 

	class salary_model extends CI_Model{

		public function get_data()
		{
			$this->db->select('*');
			$this->db->from('tbl_salary');
			$query = $this->db->get();

			return $query->result();
		}

		// this function is used to store salary

		public function store_salary()
		{
			$data = array(
				'salary_desc' =>$this->input->post('salary') ,
				'salary_staff_id' =>$this->input->post('staff'),
				'salary_date' =>$this->input->post('date_time')
				 );
			$this->db->insert('tbl_salary',$data);
			
		}
	}


 ?>