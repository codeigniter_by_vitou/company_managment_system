<?php 

	class company_m extends cI_Model{

		// this function is used to get company information
		public function get_company()
		{
			$this->db->select('*');
			$this->db->from('tbl_company');
			$query = $this->db->get();
			return $query->row();
		}
		// this function is used to count staff 
		public function count_staff()
		{	
			$this->db->select('*');
			$this->db->from('tbl_staff');
			$q = $this->db->get();
			return $q->num_rows();
		}
		// get record
	}


 ?>