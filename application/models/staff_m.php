<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class staff_m extends CI_Model{


	public function get_staff()
	{
		$this->db->select('tbl_staff.*,tbl_department.dep_title');
		$this->db->from('tbl_department');
		$this->db->join('tbl_staff', 'tbl_staff.staff_dep_id = tbl_department.dep_id ');
		$q = $this->db->get();
		return $q->result();

	}
	public function add()
	{
		$this->db->select('*');
		$this->db->from('tbl_department');
		$query = $this->db->get();
		return $query->result(); 
		// store record
	}
	public function store()
	{
		$data = array(
			'staff_name' =>$this->input->post('name'),
			'gender' =>$this->input->post('gender'),
			'email' =>$this->input->post('email'),
			'phone' =>$this->input->post('phone'),
			'website' =>$this->input->post('website'),
			'address' =>$this->input->post('address'),
			'company_id' =>'1',
			'staff_dep_id' =>$this->input->post('staff_dep_id'),

			'create_at' =>date('Y-m-d H:i:s')
 		);
		$store = $this->db->insert('tbl_staff',$data);
		if ($store) {
			$this->session->set_flashdata('sucess-msg','Success!');
		}

	}
		// edit record
	public function staff_edit($id)
	{
		$this->db->select('tbl_staff.*,tbl_company.company_name');
		$this->db->from('tbl_company');
		$this->db->join('tbl_staff', 'tbl_company.company_id = tbl_staff.company_id');
		$q = $this->db->get();
		return $q->row();
	}
		// update record
	public function staff_update()
	{
		$id = $this->input->post('id');
		$field = array(
			'staff_name' =>$this->input->post('name'),
			'gender' =>$this->input->post('gender'),
			'email' =>$this->input->post('email'),
			'phone' =>$this->input->post('phone'),
			'website' =>$this->input->post('website'),
			'address' =>$this->input->post('address'),
		);
		$this->db->where('staff_id',$id);
		$aa = $this->db->update('tbl_staff',$field);

	}
		// delete
	public function deleteInfo($id)
	{
		$this->db->where('staff_id',$id);
		$this->db->delete('tbl_staff');

	}
		// view
	public function viewInfo($id)
	{
		$this->db->select('tbl_staff.*,tbl_company.company_name');
		$this->db->from('tbl_company');
		$this->db->join('tbl_staff', 'tbl_company.company_id = tbl_staff.company_id');
		$q = $this->db->get();
		return $q->row();
	}
	// search
	public function searchInfo()
	{
		$q=$this->input->post('search_staff');
		$this->db->like('staff_name',$q);
		$this->db->or_like('phone',$q);
		$this->db->or_like('email',$q);
		$this->db->or_like('address',$q);
		$this->db->or_like('website',$q);
		$data = $this->db->get('tbl_staff');

		if ($data->num_rows()<=0 || $q == null)
		{
			$this->session->set_flashdata('error_msg','search not found');
			redirect(base_url('staffController/index'));
		}
		else
		{
			return $data->result();
		}
	}
	// delete all
	public function deleteAllCheck()
	{
		$data = $this->input->post('checkbox');
		foreach ($data as $id ) {
			$this->db->where('id',$id);
			$this->db->delete('tbl_staff');
		}

	}
	public function report_staff()
	{
		$this->db->select('*');
		$this->db->from('tbl_staff');
		$query = $this->db->get();
		return $query->result();
	}
	// this function is used to get department info

	public function report_dep()
	{
		$this->db->select('*');
		$this->db->from('tbl_department');
		$query = $this->db->get();
		return $query->result();
	}

	// this function is used to get salary info
	public function report_salary()
	{
		$this->db->select('tbl_salary.*,tbl_staff.staff_name');
		$this->db->from('tbl_salary');
		$this->db->join('tbl_staff',
			'tbl_salary.salary_staff_id = tbl_staff.staff_id');
		$query = $this->db->get();
		return $query->result();
	}
	// this function is used to get leave info
	public function report_leave()
	{
		$this->db->select('*,tbl_staff.staff_name');
		$this->db->from('tbl_leave');
		$this->db->join('tbl_staff','tbl_staff.staff_id = tbl_leave.leave_staff_id');
		$query = $this->db->get();
		return $query->result();
	}
}


?>