<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class salaryController extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('staff_m');
			$this->load->model('salary_model');
			
		}
		public function index()
		{
			$data['salaries'] = $this->salary_model->get_data();
			
			$this->load->view('salarys/index',$data);
			
		}

		// this function is used to open add view

		public function add()
		{
			$data['staffs'] = $this->staff_m->get_staff();
			$this->load->view('salarys/add',$data);
		}

		// this function is used to store staff's salary

		public function store()
		{
			// Validation For Name Field
			
			$this->form_validation->set_rules('staff', 'staff', 'required');
			$this->form_validation->set_rules('salary', 'salary', 'required');
			$this->form_validation->set_rules('date_time', 'date_time', 'required');
			if($this->form_validation->run() == FALSE){

	    		$this->add();

		    }else{

		    	$this->salary_model->store_salary();
		    	$data['success'] = 'congrate!';
		    	$data['salaries'] = $this->salary_model->get_data();
		    	$this->load->view('salarys/index',$data);
		    	
			}
		

		}


}
 ?>