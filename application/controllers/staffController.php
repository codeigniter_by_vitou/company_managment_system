<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	 * 
	 */
	class staffController extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();	
			$this->load->model('staff_m','m');
			$this->load->model('company_m');
		}
		public function index()
		{
			
			$data['staffs'] =  $this->m->get_staff();
			$data['company'] = $this->company_m->get_company();
			$this->load->view('layout/header');
			$this->load->view('staff/index',$data);
			$this->load->view('layout/footer');
		}

		public function add()
		{

			$data['departments'] = $this->m->add();
			$this->load->view('layout/header');
			$this->load->view('staff/add',$data);
			$this->load->view('layout/footer');
			
		}
		public function store()
		{
			$this->form_validation->set_rules('name','Name','required');
			$this->form_validation->set_rules('gender','Gender','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('phone','Phone','required');
			$this->form_validation->set_rules('website','Website','required');
			$this->form_validation->set_rules('address','Address','required');
			// $this->form_validation->set_rules('company_id','Company','required');

			if ($this->form_validation->run() == FALSE) {

				$this->add();

			}else{

				$data = $this->m->store();
				$this->session->set_flashdata('success_msg', 'Record added successfully');
				redirect(base_url('staffController/staff_management'));
			}
			
		}
		public function edit($id)
		{
			$data['staff'] = $this->m->staff_edit($id);
			$this->load->view('staff/edit',$data);

		}
		// update
		public function update()
		{
			
			$this->m->staff_update();
			$this->session->set_flashdata('success_msg','Record updated successfully');
			redirect(base_url('staffController/staff_management'));
		
		}
		// delete
		public function delete($id)
		{
			$this->m->deleteInfo($id);
			$this->session->set_flashdata('success_msg', 'Record deleted successfully');
			redirect(base_url('staffController/staff_management'));
		}
		// view
		public function view($id)
		{
			$data['staff'] =  $this->m->viewInfo($id);
			$this->load->view('layout/header');
			$this->load->view('staff/view',$data);
			$this->load->view('layout/footer');
		}
		//search
		public function search()
		{
			$data['search_staff'] = $this->m->searchInfo();
			$this->load->view('staff/index',$data);
			
		}
		// delete all
		public function deleteAll()
		{
		
			$this->m->deleteAllCheck();
			$this->session->set_flashdata('success_msg','Delete successfully');
			redirect(base_url('staffController/staff_management'));
		
		}
		public function report()
		{
			$data['report_dep'] = $this->m->report_dep();
			$data['report_leave'] = $this->m->report_leave();
			$data['report_salary'] = $this->m->report_salary();
			$data['reports'] = $this->m->report_staff();

			$this->load->view('company/report',$data);
		}

		public function staff_management()
		{

			
			$data['staffs'] = $this->m->get_staff();
			
			$this->load->view('company/staff_management',$data);
		
		}

	}


	?>