<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class departmentController extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('staff_m');
			$this->load->model('department_model');
			
		}
		public function index()
		{
			$data['departments'] = $this->department_model->get_data();
			
			$this->load->view('departments/index',$data);
			
		}

		// this function is used to open add view

		public function add()
		{
			$this->load->view('departments/add');
		}

		// this function is used to store staff's department

		public function store()
		{
			// Validation For Name Field
			
			$this->form_validation->set_rules('department_title', 'department_title', 'required');
			$this->form_validation->set_rules('department_desc', 'department_desc', 'required');
			if($this->form_validation->run() == FALSE){

	    		$this->add();

		    }else{

		    	$this->department_model->store_department();
		    	$data['departments'] = $this->department_model->get_data();
		    	$data['success'] = 'congrate!';
		    	$this->load->view('departments/index',$data);
		    	
			}
		

		}


}
 ?>