<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class departementController extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('staff_m');
			$this->load->model('departments_model');
			
		}
		public function index()
		{
			$data['departments'] = $this->->get_data();
			
			$this->load->view('departments/index',$data);
			
		}

		// this function is used to open add view

		public function add()
		{
			$data['staffs'] = $this->staff_m->get_staff();
			$this->load->view('departments/add',$data);
		}

		// this function is used to store staff's departement

		public function store()
		{
			// Validation For Name Field
			
			$this->form_validation->set_rules('department_title', 'department_title', 'required');
			$this->form_validation->set_rules('department_desc', 'department_desc', 'required');
			if($this->form_validation->run() == FALSE){

	    		$this->add();

		    }else{

		    	$this->departments_model->store_company();
		    	$data['success'] = 'congrate!';
		    	$this->load->view('departments/index',$data);
		    	
			}
		

		}


}
 ?>