<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class LeaveController extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('staff_m');
			$this->load->model('Leave_Model');
			
		}
		public function index()
		{
			
			$data['leaves'] = $this->Leave_Model->get_data();
			$data['success'] = 'congrate!';
		    $data['leaves'] = $this->Leave_Model->get_data();


			$this->load->view('Leaves/index',$data);
			
		}

		// this function is used to open add view

		public function add()
		{
			$data['staffs'] = $this->staff_m->get_staff();
			$this->load->view('layout/header');
			$this->load->view('leaves/add',$data);
			$this->load->view('layout/footer');
		}

		// this function is used to store staff's Leave

		public function store()
		{
			// Validation For Name Field
			
			$this->form_validation->set_rules('staff', 'staff', 'required');
			$this->form_validation->set_rules('date_time', 'date_time', 'required');
			
			if($this->form_validation->run() == FALSE){

	    		$this->add();

		    }else{

		    	$this->Leave_Model->store_Leave();
		    	
		    	$this->index();
		    	
			}
		

		}


}
 ?>