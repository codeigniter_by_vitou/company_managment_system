



<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" >
	
	<title>Customer</title>
	<style type="text/css">
	.error{
		color: red;
	}
	.container{
		padding-left: 300px;
	}
	</style>
	
</head>
<body>
	
	<div class="container" style="background: #eee">
		<div class="row"><head><h1>សូមបំពេញព័ត៌មានរបស់អ្នកនូវខាងក្រេាម!</h1></head></div>
		<div class="row"><nav><div class="error"><?php echo validation_errors(); ?></div></nav></div>
		<div class="row">
			<content>
				<form action="<?php echo base_url('customerController/customer_store') ?>" method="POST">
					<div class="form-group row">
					  <label for="example-text-input" class="col-2 col-form-label">Name:</label>
					  <div class="col-10">
					    <input class="form-control" type="text" value="" id="example-text-input" placeholder="khmer angkor" name="cname">
					  </div>
					</div>
					<div class="form-group row">
					  <label for="example-tel-input" class="col-2 col-form-label">Tel:</label>
					  <div class="col-10">
					    <input class="form-control" type="tel" value="" id="example-tel-input" placeholder="(+885)-555-5555" name="cmobile">
					    <span style="color: #909">example: 0962359162</span>
					  </div>
					</div>
					<div class="form-group">
						<input type="submit" name="" value="Save" class="btn btn-primary">
					</div>
				</form>	
			</content>
		</div>
		<div class="row">
			<?php 
	         echo $this->session->flashdata('email_sent'); 
	         echo form_open('customerController/send_mail'); 
	      	?> 
				
		      <input type = "email" name = "email" required /> 
		      <input type = "submit" value = "SEND MAIL"> 
			
	      	<?php 
		         echo form_close(); 
		     ?> 

		</div>
		<div class="row"><footer>footer</footer></div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.slim.min.js'); ?> "></script>
	<script src="<?php echo base_url('assets/js/popper.js/1.14.3/umd/popper.min.js'); ?> "></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?> " ></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		<?php
		if($this->session->flashdata('success_msg')){ ?>		
			swal({
			  title: "សូមចូលរួមអបអរសាទរ!",
			  text: "ក្រុមការងារយីេងខ្ញុំនិងធ្វេីការទំនាក់ទំនង ទៅអ្នកវិញក្នងរយះពេល១មេាំងនៃម៉ាេងធ្វេីការ។ សូមអរគុណ!",
			  icon: "success",
			  button: "OK !",
			});
		<?php } ?>		
 	</script>
</body>
</html>