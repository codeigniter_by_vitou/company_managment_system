<!DOCTYPE html>
<html>
<head>
	<title>report</title>
</head>
<body>
	<div class="content">
		<!-- department report -->
		<h1>department Reports</h1>
		<table border="1px;" cellpadding="5" cellspacing="0">
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Description</th>
				<th>Create_at</th>
			</tr>
			<?php foreach( $report_dep as $report){ ?>
			<tr>
				<td><?php echo $report->dep_id ; ?></td>
				<td><?php echo $report->dep_title ; ?></td>
				<th><?php echo $report->dep_desc; ?></th>
				<th><?php echo $report->dep_create_at; ?></th>
			</tr>
			<?php } ?>
		</table>
		<!-- staff report -->
		<h1>Staff Reports</h1>
		<table border="1px;" cellpadding="5" cellspacing="0">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Start_working</th>
			</tr>
			<?php foreach( $reports as $report){ ?>
			<tr>
				<td><?php echo $report->staff_id ; ?></td>
				<td><?php echo $report->staff_name ; ?></td>
				<th><?php echo $report->email; ?></th>
				<th><?php echo $report->phone; ?></th>
				<th><?php echo $report->create_at; ?></th>
			</tr>
			<?php } ?>
		</table>
		<!-- salary report-->
		<h1>salary Reports</h1>
		<table border="1px;" cellpadding="5" cellspacing="0">
			<tr>
				<th>ID</th>
				<th>Staff's Name</th>
				<th>Money($)</th>
				<th>Date</th>
			</tr>
			<?php foreach( $report_salary as $report){ ?>
			<tr>
				<td><?php echo $report->salary_id ; ?></td>
				<td><?php echo $report->staff_name ; ?></td>
				<th><?php echo $report->salary_desc; ?></th>
				<th><?php echo $report->salary_date; ?></th>
			</tr>
			<?php } ?>
		</table>
		<!-- leave report -->
		<h1>leave Reports</h1>
		<table border="1px;" cellpadding="5" cellspacing="0">
			<tr>
				<th>ID</th>
				<th>Staff's Name</th>
				<th>Date</th>
			</tr>
			<?php foreach( $report_leave as $report){ ?>
			<tr>
				<td><?php echo $report->leave_id ; ?></td>
				<td><?php echo $report->staff_name ; ?></td>
				<td><?php echo $report->leave_date ; ?></td>
				
			</tr>
			<?php } ?>
		</table>

	</div>
	
</body>
</html>