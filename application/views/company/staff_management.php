<!DOCTYPE html>
<html>
<head>
	<title>staff_management</title>
	<style type="text/css">
		.container{
			margin: 50px 0px 0px 150px;
		}
	</style>

</head>
<body>
	<?php
	if($this->session->flashdata('success_msg'))
	{
		?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
		<?php		
	}
	?>
	<div class="container">
		<a href="<?php echo base_url('companyController/index') ?>">Back</a>
		<div class="row"><a href="<?php echo base_url('staffController/add') ?>"> +Add </a></div>
		<div class="row">
			<table border="1px" cellspacing="0" cellpadding="2">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Departments</th>
					<th>Action</th>
				</tr>
				<?php foreach( $staffs as $staff){ ?>
				<tr>
					<td><?php echo $staff->staff_id ; ?></td>
					<td><?php echo $staff->staff_name ; ?></td>
					<th><?php echo $staff->email; ?></th>
					<th><?php echo $staff->phone; ?></th>
					<th><?php echo $staff->dep_title; ?></th>
					<th>
						<a href="<?php echo base_url('staffController/edit/'.$staff->staff_id) ?>">Edit</a>|
						<a href="<?php echo base_url('staffController/delete/'.$staff->staff_id) ?>" onclick="return confirm_delete()">delete</a>|
						<a href="<?php echo base_url('staffController/view/'.$staff->staff_id) ?>">view</a>|
					</th>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>


	<!-- script -->
	<script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
	<script type="text/javascript">
	function confirm_delete() {
		return confirm('are you sure?');
	}
</script>
<script>
	$(document).ready(function() {
		$("#checkedAll").change(function(){
			if(this.checked){
				$(".checkSingle").each(function(){
					this.checked=true;
				})              
			}else{
				$(".checkSingle").each(function(){
					this.checked=false;
				})              
			}
		});

		$(".checkSingle").click(function () {
			if ($(this).is(":checked")){
				var isAllChecked = 0;
				$(".checkSingle").each(function(){
					if(!this.checked)
						isAllChecked = 1;
				})              
				if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }     
			}else {
				$("#checkedAll").prop("checked", false);
			}
		});
	});
</script>
</body>
</html>