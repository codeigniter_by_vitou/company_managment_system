
	<div class="container">
		<a href="<?php echo base_url('LeaveController/index') ?>">Back</a>
		<div class="error"><?php echo validation_errors(); ?></div>
		<div class="row">
			<form action="<?php echo base_url('LeaveController/store') ?>" method="POST">
				<label class="mr-sm-2" for="inlineFormCustomSelect">Staffs</label>
				<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect" name="staff">
					<option selected> choose... </option>
					<?php  foreach( $staffs as $staff){ ?>
					<option value="<?php echo $staff->staff_id; ?>"><?php echo $staff->staff_name; ?></option>
					<?php  } ?>
					
				</select>
				<br/>
				<div class="row">
					<label class="mr-sm-2" for="inlineFormCustomSelect">Date and Time</label>
					 <input class="form-control" type="datetime-local"  id="example-datetime-local-input" name="date_time" value="2018-08-19T13:45:00">
				</div>
				<input type="submit" name="" value="Save">
			</form>
		</div>
	</div>
