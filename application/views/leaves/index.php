<!DOCTYPE html>
<html>
<head>
	<title>leave</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" >
	
	<style type="text/css">
	.container{
		margin: 50px 0px 0px 150px;
	}
	.success{
		color:blue;
	}
</style>

</head>
<body>
	<?php
	if($this->session->flashdata('success_msg'))
	{
		?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
		<?php		
	}
	?>
	<div class="container">
		<div class="success">
			<h1>
				<?php if (isset($success)) {?>
					<?php echo $success; ?>
				<?php } ?>
			</h1>
		</div>
		<a href="<?php echo base_url('companyController/index') ?>">Back</a>
		<div class="row"><a href="<?php echo base_url('LeaveController/add') ?>"> +Add </a></div>
		<div class="row" style="text-align: center;">
			<table border="1px" cellspacing="0" cellpadding="2">
				<tr>
					<th>ID</th>
					<th>staff_name</th>
					<th>leave_date</th>
					<th>Action</th>
				</tr>
				<?php foreach( $leaves as $leave) {?>
				<tr>
					<td><?php echo $leave->leave_id; ?></td>
					<td><?php echo $leave->staff_name; ?></td>
					<td><?php echo $leave->leave_date; ?></td>
					<td>
						<a href="#">edit</a>|
						<a href="#">view</a>|
						<a href="#">delete</a>
					</td>
					<td></td>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>


	<!-- script -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.slim.min.js'); ?> "></script>
	<script src="<?php echo base_url('assets/js/popper.js/1.14.3/umd/popper.min.js'); ?> "></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?> " ></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		function confirm_delete() {
			return confirm('are you sure?');
		}
	</script>
</body>
</html>