<!DOCTYPE html>
<html>
<head>
	<title>add</title>
	<style type="text/css">
	.container{
		margin: 50px 0px 0px 150px;
	}
	.error{
		color: red;
	}
</style>
</head>
<body>
	<div class="container">
		<a href="<?php echo base_url('companyController/index') ?>">Back</a>
		<div class="error"><?php echo validation_errors(); ?></div>
		<div class="row">
			<form action="<?php echo base_url('companiesController/store') ?>" method="POST">
				
				<div class="row">
				<label class="mr-sm-2" for="inlineFormCustomSelect">Company Name</label>
				<input type="text" name="company_name" >
				</div>
				<div class="row">
				<label class="mr-sm-2" for="inlineFormCustomSelect">Company Email</label>
				<input type="email" name="company_email" >
				</div>
				<div class="row">
				<label class="mr-sm-2" for="inlineFormCustomSelect">Company Address</label>
				<input type="text" name="company_address" >
				</div>
				<input type="submit" name="" value="Save">
			</form>
		</div>
	</div>


</body>
</html>