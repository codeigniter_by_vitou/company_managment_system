<!DOCTYPE html>
<html>
<head>
	<title>add</title>
	<style type="text/css">
	.container{
		margin: 50px 0px 0px 150px;
	}
	.error{
		color: red;
	}
</style>
</head>
<body>
	<div class="container">
		<a href="<?php echo base_url('departmentController/index') ?>">Back</a>
		<div class="error"><?php echo validation_errors(); ?></div>
		<div class="row">
			<form action="<?php echo base_url('departmentController/store') ?>" method="POST">
				
				
				<div class="row">
					<label>department_title</label>
					<input type="text" name="department_title">
				</div>
				
				<div class="row">
					<label class="mr-sm-2" for="inlineFormCustomSelect">Description </label>
					<textarea name="department_desc"></textarea>
				</div>
				<input type="submit" name="" value="Save">
			</form>
		</div>
	</div>


</body>
</html>