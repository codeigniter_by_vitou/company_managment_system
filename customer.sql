-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2018 at 02:11 PM
-- Server version: 5.7.11
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer`
--

-- --------------------------------------------------------

--
-- Table structure for table `custormers`
--

CREATE TABLE `custormers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(60) NOT NULL,
  `customer_mobile` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custormers`
--

INSERT INTO `custormers` (`customer_id`, `customer_name`, `customer_mobile`) VALUES
(1, 'rithy', '0962359162'),
(2, 'rithy', '0962359162'),
(3, 'rithy', '0962359162'),
(4, 'rithy', '0962359162'),
(5, 'rithy', '0962359162'),
(6, 'rithy', '0962359162'),
(7, 'rithy', '0962359162'),
(8, 'vitou', '0962359162'),
(9, 'vitou', '0962359162'),
(10, 'vitou', '0962359162'),
(11, 'vitou', '0962359162'),
(12, 'vitou', '0962359162'),
(13, 'vitou', '0962359162'),
(14, 'vitou', '0962359162'),
(15, 'vitou', '0962359162'),
(16, 'vitou', '0962359162'),
(17, 'vitou', '0962359162'),
(18, 'vitou', '0962359162'),
(19, 'vitou', '0962359162'),
(20, 'vitou', '0962359162'),
(21, 'vitou', '0962359162'),
(22, 'vitou', '0962359162'),
(23, 'vitou', '0962359162'),
(24, 'vitou', '0962359162'),
(25, 'vitou', '0962359162'),
(26, 'vitou', '0962359162'),
(27, 'vitou', '0962359162'),
(28, 'rithy', '12321321312'),
(29, 'rithy', '12321321312'),
(30, 'rithy', '12321321312'),
(31, 'rithy', '12321321312'),
(32, 'rithy', '12321321312'),
(33, 'rithy', '12321321312'),
(34, 'rithy', '12321321312'),
(35, 'rithy', '12321321312');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `custormers`
--
ALTER TABLE `custormers`
  ADD PRIMARY KEY (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `custormers`
--
ALTER TABLE `custormers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
